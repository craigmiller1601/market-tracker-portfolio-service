package us.craigmiller160.markettracker.portfolio.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.r2dbc.core.DatabaseClient
import us.craigmiller160.markettracker.portfolio.domain.client.CoroutineDatabaseClient

@Configuration
class DatabaseConfig {
  @Bean
  fun coroutineDatabaseClient(databaseClient: DatabaseClient): CoroutineDatabaseClient =
      CoroutineDatabaseClient(databaseClient)
}
