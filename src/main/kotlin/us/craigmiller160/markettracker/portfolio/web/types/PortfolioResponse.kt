package us.craigmiller160.markettracker.portfolio.web.types

import us.craigmiller160.markettracker.portfolio.common.typedid.PortfolioId
import us.craigmiller160.markettracker.portfolio.common.typedid.TypedId

data class PortfolioResponse(
    val id: TypedId<PortfolioId>,
    val name: String,
    val stockSymbols: List<String>
)
