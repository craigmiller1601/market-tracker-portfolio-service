package us.craigmiller160.markettracker.portfolio.web.routes

import org.springframework.context.annotation.Bean
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.server.RouterFunction
import org.springframework.web.reactive.function.server.ServerResponse
import us.craigmiller160.markettracker.portfolio.web.handlers.DownloadHandler
import us.craigmiller160.markettracker.portfolio.web.swagger.addNoContentResponse
import us.craigmiller160.markettracker.portfolio.web.swagger.coSwaggerRouter

@Component
class DownloadRoutesHolder {
  @Bean
  fun downloadRoutes(handler: DownloadHandler): RouterFunction<ServerResponse> = coSwaggerRouter {
    POST("/download", handler::download) { it.operationId("download").addNoContentResponse() }
  }
}
