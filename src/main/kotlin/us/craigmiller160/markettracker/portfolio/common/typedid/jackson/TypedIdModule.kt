package us.craigmiller160.markettracker.portfolio.common.typedid.jackson

import com.fasterxml.jackson.databind.module.SimpleModule
import org.springframework.stereotype.Component
import us.craigmiller160.markettracker.portfolio.common.typedid.TypedId

@Component
class TypedIdModule : SimpleModule("TypedIdModule") {
  init {
    addSerializer(TypedIdSerializer())
    addDeserializer(TypedId::class.java, TypedIdDeserializer())
  }
}
