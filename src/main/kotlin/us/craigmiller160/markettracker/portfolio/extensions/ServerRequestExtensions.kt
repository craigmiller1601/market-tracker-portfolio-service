package us.craigmiller160.markettracker.portfolio.extensions

import arrow.core.Either
import arrow.core.getOrElse
import arrow.core.raise.either
import org.springframework.web.reactive.function.server.ServerRequest
import us.craigmiller160.markettracker.portfolio.web.exceptions.BadRequestException
import us.craigmiller160.markettracker.portfolio.web.exceptions.MissingParameterException

fun <T> ServerRequest.pathVariable(name: String, parser: (String) -> T): T =
    Either.catch { pathVariable(name).let(parser) }
        .getOrElse { throw BadRequestException("Error parsing path variable $name", it) }

fun <T> ServerRequest.requiredQueryParam(name: String, parser: (String) -> T): T =
    either {
          val paramString =
              queryParam(name).leftIfEmpty().mapLeft { MissingParameterException(name) }.bind()
          Either.catch { parser(paramString) }
              .mapLeft { BadRequestException("Error parsing $name", it) }
              .bind()
        }
        .getOrElse { throw it }

fun <T> ServerRequest.optionalQueryParam(name: String, parser: (String) -> T): T? =
    queryParam(name).map { parser(it) }.orElse(null)
