package us.craigmiller160.markettracker.portfolio.domain.models

import us.craigmiller160.markettracker.portfolio.common.typedid.PortfolioId
import us.craigmiller160.markettracker.portfolio.common.typedid.TypedId
import us.craigmiller160.markettracker.portfolio.common.typedid.UserId
import us.craigmiller160.markettracker.portfolio.web.types.PortfolioResponse

interface Portfolio {
  val id: TypedId<PortfolioId>
  val userId: TypedId<UserId>
  val name: String
}

fun Portfolio.toPortfolioResponse(stockSymbols: List<String>): PortfolioResponse =
    PortfolioResponse(id = this.id, name = this.name, stockSymbols)
