package us.craigmiller160.markettracker.portfolio.domain.models

import java.math.BigDecimal
import java.time.LocalDate
import us.craigmiller160.markettracker.portfolio.common.typedid.PortfolioId
import us.craigmiller160.markettracker.portfolio.common.typedid.TypedId
import us.craigmiller160.markettracker.portfolio.common.typedid.UserId
import us.craigmiller160.markettracker.portfolio.web.types.SharesOwnedResponse

data class SharesOwnedOnDate(
    val userId: TypedId<UserId>,
    val date: LocalDate,
    val symbol: String,
    val totalShares: BigDecimal,
    val portfolioId: TypedId<PortfolioId>? = null
)

fun SharesOwnedOnDate.toSharesOwnedResponse(): SharesOwnedResponse =
    SharesOwnedResponse(date = date, totalShares = totalShares)
