package us.craigmiller160.markettracker.portfolio.domain.rowmappers

import arrow.core.raise.either
import java.util.UUID
import us.craigmiller160.markettracker.portfolio.common.typedid.TypedId
import us.craigmiller160.markettracker.portfolio.domain.models.BasePortfolio
import us.craigmiller160.markettracker.portfolio.domain.models.Portfolio

val portfolioRowMapper: RowMapper<Portfolio> = { row ->
  either {
    val id = row.getRequired("id", UUID::class).bind()
    val userId = row.getRequired("user_id", UUID::class).bind()
    val name = row.getRequired("name", String::class).bind()

    BasePortfolio(id = TypedId(id), userId = TypedId(userId), name = name)
  }
}
