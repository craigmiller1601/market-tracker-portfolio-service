package us.craigmiller160.markettracker.portfolio.domain.rowmappers

import us.craigmiller160.markettracker.portfolio.domain.client.Row
import us.craigmiller160.markettracker.portfolio.extensions.TryEither

typealias RowMapper<T> = (Row) -> TryEither<T>
