package us.craigmiller160.markettracker.portfolio.domain.models

import us.craigmiller160.markettracker.portfolio.common.typedid.PortfolioId
import us.craigmiller160.markettracker.portfolio.common.typedid.TypedId
import us.craigmiller160.markettracker.portfolio.common.typedid.UserId

data class PortfolioWithHistory(
    override val id: TypedId<PortfolioId>,
    override val userId: TypedId<UserId>,
    override val name: String,
    val ownershipHistory: List<SharesOwned>
) : Portfolio
