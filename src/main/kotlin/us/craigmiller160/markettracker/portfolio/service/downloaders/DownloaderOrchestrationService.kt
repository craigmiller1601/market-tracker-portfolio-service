package us.craigmiller160.markettracker.portfolio.service.downloaders

import arrow.core.flatMap
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import us.craigmiller160.markettracker.portfolio.extensions.TryEither
import us.craigmiller160.markettracker.portfolio.service.downloaders.craigmiller.CraigMillerDownloaderService

@Service
class DownloaderOrchestrationService(
    private val craigMillerDownloaderService: CraigMillerDownloaderService,
    private val persistDownloadService: PersistDownloadService
) {
  private val log = LoggerFactory.getLogger(javaClass)

  suspend fun download(): TryEither<Unit> {
    log.info("Beginning to download all portfolio data")
    return craigMillerDownloaderService
        .download()
        .flatMap { persistDownloadService.persistPortfolios(it) }
        .map { Unit }
        .onRight { log.info("Finished downloading all portfolio data") }
        .onLeft { ex -> log.error("Error downloading or persisting portfolio data", ex) }
  }
}
