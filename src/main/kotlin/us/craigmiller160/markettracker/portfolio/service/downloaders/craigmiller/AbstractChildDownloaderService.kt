package us.craigmiller160.markettracker.portfolio.service.downloaders.craigmiller

import org.slf4j.LoggerFactory
import org.springframework.web.reactive.function.client.WebClient
import us.craigmiller160.markettracker.portfolio.config.CraigMillerDownloaderConfig
import us.craigmiller160.markettracker.portfolio.config.PortfolioConfig
import us.craigmiller160.markettracker.portfolio.extensions.TryEither
import us.craigmiller160.markettracker.portfolio.extensions.awaitBodyResult
import us.craigmiller160.markettracker.portfolio.extensions.retrieveSuccess

typealias DownloadSpreadsheetResult = TryEither<Pair<PortfolioConfig, GoogleSpreadsheetValues>>

abstract class AbstractChildDownloaderService(
    private val downloaderConfig: CraigMillerDownloaderConfig,
    private val webClient: WebClient
) : ChildDownloaderService {
  private val log = LoggerFactory.getLogger(javaClass)

  protected suspend fun downloadSpreadsheet(
      config: PortfolioConfig,
      accessToken: String
  ): DownloadSpreadsheetResult {
    log.debug(
        "Downloading data from spreadsheet. Sheet: ${config.sheetId} Values: ${config.valuesRange}")
    return webClient
        .get()
        .uri(
            "${downloaderConfig.googleSheetsApiBaseUrl}/spreadsheets/${config.sheetId}/values/${config.valuesRange}")
        .header("Authorization", "Bearer $accessToken")
        .retrieveSuccess()
        .awaitBodyResult<GoogleSpreadsheetValues>()
        .map { config to it }
  }
}
