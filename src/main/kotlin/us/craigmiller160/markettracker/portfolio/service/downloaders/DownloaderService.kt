package us.craigmiller160.markettracker.portfolio.service.downloaders

import us.craigmiller160.markettracker.portfolio.domain.models.PortfolioWithHistory
import us.craigmiller160.markettracker.portfolio.extensions.TryEither

interface DownloaderService {
  suspend fun download(): TryEither<List<PortfolioWithHistory>>
}
