package us.craigmiller160.markettracker.portfolio.service.downloaders.craigmiller

import us.craigmiller160.markettracker.portfolio.domain.models.PortfolioWithHistory
import us.craigmiller160.markettracker.portfolio.extensions.TryEither

typealias ChildDownloadServiceResult = TryEither<List<PortfolioWithHistory>>

interface ChildDownloaderService {
  suspend fun download(token: String): ChildDownloadServiceResult
}
