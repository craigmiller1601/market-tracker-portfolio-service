package us.craigmiller160.markettracker.portfolio.service

import kotlinx.coroutines.reactive.awaitSingle
import org.springframework.security.core.context.ReactiveSecurityContextHolder
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.stereotype.Service
import us.craigmiller160.markettracker.portfolio.common.typedid.TypedId
import us.craigmiller160.markettracker.portfolio.common.typedid.UserId
import us.craigmiller160.markettracker.portfolio.common.typedid.toTypedId

@Service
class AuthorizationService {
  suspend fun getUserId(): TypedId<UserId> =
      ReactiveSecurityContextHolder.getContext()
          .awaitSingle()
          .authentication
          .let { it.principal as Jwt }
          .let { it.subject.toTypedId() }
}
