package us.craigmiller160.markettracker.portfolio.testcontainer

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import us.craigmiller160.markettracker.portfolio.web.exceptions.BadRequestException
import us.craigmiller160.markettracker.portfolio.web.exceptions.MissingParameterException

@RestController
@RequestMapping("/throw-exception")
class ExceptionThrowingController {
  @GetMapping("/runtime-exception")
  fun runtimeException() {
    throw RuntimeException("Dying")
  }

  @GetMapping("/missing-parameter")
  fun missingParameter() {
    throw MissingParameterException("stuff")
  }

  @GetMapping("/bad-request")
  fun badRequest() {
    throw BadRequestException("Dying")
  }
}
