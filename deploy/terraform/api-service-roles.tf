data "keycloak_openid_client" "market_tracker_api_client_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  client_id = "market-tracker-api"
}

data "keycloak_openid_client" "market_tracker_api_client_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  client_id = "market-tracker-api"
}

import {
  to = keycloak_openid_client_service_account_role.market_tracker_api_service_access_dev
  id = "apps-dev/20603e11-49aa-4640-b403-9fbb2934f3c9/317db42f-e456-4f67-8445-58898eb0f12c/816a4bec-b308-403c-a071-ead516008ccb"
}

resource "keycloak_openid_client_service_account_role" "market_tracker_api_service_access_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  service_account_user_id = keycloak_openid_client.market_tracker_portfolio_service_dev.service_account_user_id
  client_id = data.keycloak_openid_client.market_tracker_api_client_dev.id
  role = local.access_role_common.name
}

import {
  to = keycloak_openid_client_service_account_role.market_tracker_api_service_access_prod
  id = "apps-prod/803ce06d-780b-4456-8c8c-35cc14953b68/95994ef7-2cf9-410d-915f-6ccc65a97370/68c593c3-786a-4b8a-a7ab-ad47bb1da610"
}

resource "keycloak_openid_client_service_account_role" "market_tracker_api_service_access_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  service_account_user_id = keycloak_openid_client.market_tracker_portfolio_service_prod.service_account_user_id
  client_id = data.keycloak_openid_client.market_tracker_api_client_prod.id
  role = local.access_role_common.name
}