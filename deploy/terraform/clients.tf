data "keycloak_realm" "apps_dev" {
  realm = "apps-dev"
}

data "keycloak_realm" "apps_prod" {
  realm = "apps-prod"
}

locals {
  client_common = {
    client_id = "market-tracker-portfolio-service"
    name = "market-tracker-portfolio-service"
    enabled = true
    access_type = "CONFIDENTIAL"
    service_accounts_enabled = true
  }
}

import {
  to = keycloak_openid_client.market_tracker_portfolio_service_dev
  id = "apps-dev/0e5fca6f-3807-4909-ab61-8357336cf9a3"
}

resource "keycloak_openid_client" "market_tracker_portfolio_service_dev" {
  realm_id = data.keycloak_realm.apps_dev.id
  client_id = local.client_common.client_id
  name = local.client_common.name
  enabled = local.client_common.enabled
  access_type = local.client_common.access_type
  service_accounts_enabled = local.client_common.service_accounts_enabled
}

import {
  to = keycloak_openid_client.market_tracker_portfolio_service_prod
  id = "apps-prod/118ec733-712e-4320-bd1c-7816c9403fb3"
}

resource "keycloak_openid_client" "market_tracker_portfolio_service_prod" {
  realm_id = data.keycloak_realm.apps_prod.id
  client_id = local.client_common.client_id
  name = local.client_common.name
  enabled = local.client_common.enabled
  access_type = local.client_common.access_type
  service_accounts_enabled = local.client_common.service_accounts_enabled
}